import re
from collections import Counter, OrderedDict
from pprint import pprint
from datetime import datetime


class FrequencyDict:
    sentence_regex = None

    def __init__(self, file_name, regex):
        self.file_name = file_name
        self.regex = regex
        self.results = dict()
        self.sorted = None

    def get_frequency(self):
        with open(self.file_name) as file:
            for line in file:
                line_matches = re.findall(self.regex, line)
                counted_dict = dict(Counter(line_matches))
                for key, value in counted_dict.items():
                    last_value = self.results.get(key)
                    if last_value:
                        self.results[key] = last_value + value
                    else:
                        self.results[key] = value

    def sort(self):
        self.sorted = sorted(self.results.items(), key=lambda x: x[1], reverse=True)

    def find_sentence_with_word(self, word):
        regex = f'[А-Яа-я," ]+{word}[А-Яа-я," ]+\.'
        with open(self.file_name) as file:
            matches = re.findall(regex, file.read(), re.MULTILINE)
            print(len(matches))
            print(matches)


if __name__ == '__main__':
    file_name = 'example.txt'
    regex = r'[А-я]+'

    start = datetime.now()

    fd = FrequencyDict(file_name, regex)
    fd.get_frequency()
    fd.sort()

    enumerated_sort = [(counter, word[0], word[1]) for counter, word in enumerate(fd.sorted, 1)]
    pprint(enumerated_sort)

    word = 'служить'
    fd.find_sentence_with_word(word)

    end = datetime.now()

    print(f'time spent {end - start}')
